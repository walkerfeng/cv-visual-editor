import { createVisualEditorConfig } from '@/packages/visual-editor.utils'
import { ElButton, ElInput, ElOption, ElSelect } from 'element-plus'
import { NumberRange } from '@/packages/components/number-range/number-range'

import {
  createEditorColor,
  createEditorInputProp,
  createEditorSelectorProp,
  createEditorTableProps,
} from './packages/visual-editor.props'

export const visualConfig = createVisualEditorConfig()

visualConfig.registry('text', {
  label: '文本',
  preview: () => '预览文本',
  render: ({ props }) => <span style={{ color: props.color, fontSize: props.size }}>{props.text || '默认文本'}</span>,
  props: {
    text: createEditorInputProp('显示文本'),
    color: createEditorColor('字体颜色'),
    size: createEditorSelectorProp('字体大小', [
      { label: '14px', val: '14px' },
      { label: '18px', val: '18px' },
      { label: '24px', val: '24px' },
    ]),
  },
})

visualConfig.registry('button', {
  label: '按钮',
  preview: () => <ElButton>按钮preview</ElButton>,
  render: ({ props, size, custom }) => {
    
    return (
      <ElButton
        {...custom}
        type={props.type}
        size={props.size}
        style={{
          height: size.height ? `${size.height}px` : null,
          width: size.width ? `${size.width}px` : null,
        }}
      >
        {props.text || '默认文本'}
      </ElButton>
    )
  },
  props: {
    text: createEditorInputProp('显示文本'),
    type: createEditorSelectorProp('按钮类型', [
      { val: 'primary', label: '基础' },
      { val: 'success', label: '成功' },
      { val: 'warning', label: '警告' },
      { val: 'danger', label: '危险' },
      { val: 'info', label: '提示' },
      { val: 'text', label: '文本' },
    ]),
    size: createEditorSelectorProp('按钮大小', [
      { label: '默认', val: '' },
      { label: '中等', val: 'medium' },
      { label: '小', val: 'small' },
    ]),
  },
  resize: {
    height: true,
    width: true,
  },
})

visualConfig.registry('select', {
  label: '下拉框',
  preview: () => <ElSelect></ElSelect>,
  render: ({ props, model, custom }) => (
    <ElSelect key={(props.options || []).map((opt: any) => opt.value).join(',')} {...model.default} {...custom}>
      {(props.options || []).map((opt: { label: string; value: string }, index: number) => (
        <ElOption label={opt.label} value={opt.value} key={index}></ElOption>
      ))}
    </ElSelect>
  ),
  props: {
    options: createEditorTableProps('下拉选项', {
      options: [
        { label: '显示值', field: 'label' },
        { label: '绑定值', field: 'value' },
        { label: '备注', field: 'comments' },
      ],
      showKey: 'label',
    }),
  },
  model: {
    default: '绑定字段',
  },
})

visualConfig.registry('input', {
  label: '输入框',
  preview: () => <ElInput modelValue={''}></ElInput>,
  render: ({ props,model, size, custom }) => {
    return (
      <ElInput
        {...custom}
        {...model.default}
        placeholder={props.text}
        style={{ width: size.width ? `${size.width}px` : null }}
      ></ElInput>
    )
  },
  props: {
    text: createEditorInputProp('placeholder'),
  },
  model: {
    default: '绑定字段',
  },
  resize: {
    width: true,
  },
})

visualConfig.registry('number-range', {
  label: '数字范围输入框',
  preview: () => <NumberRange style={{ width: '100%' }}></NumberRange>,
  render: ({ model, size }) => (
    <NumberRange
      style={{ width: size.width ? `${size.width}px` : null}}
      {...{
        start: model.start.value,
        'onUpdate:start': model.start.onChange,
        end: model.end.value,
        'onUpdate:end': model.end.onChange,
      }}
    ></NumberRange>
  ),
  model: {
    start: '起始绑定字段',
    end: '截止绑定字段',
  },
  resize: {
    width: true,
  },
})


visualConfig.registry('image', {
  label: '图片',
  resize: {
    width: true,
    height: true,
  },
  render: ({ props, size }) => {
    return (
      <div style={{ height: `${size.height || 100}px`, width: `${size.width || 100}px` }} class="visual-block-image">
        <img src={props.url || 'https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png'} />
      </div>
    )
  },
  preview: () => (
    <div>
      <i style="font-size:30px;color:#999;" class="el-icon-picture" />
    </div>
  ),
  props: {
    url: createEditorInputProp('地址'),
  },
})