import useCommand from './plugins/commander.plugin'
import deepcopy from 'deepcopy'
import { VisualEditorBlockData, VisualEditorModelValue } from './visual-editor.utils'

interface CommandVisualParams {
  dataModel: { value: VisualEditorModelValue }
  updateBlocks: (blocks?: VisualEditorBlockData[]) => void
  focusData: {
    value: {
      focus: VisualEditorBlockData[]
      unFocus: VisualEditorBlockData[]
    }
  }
  dragstart: {
    on: (cb: () => void) => void
    off: (cb: () => void) => void
  }
  dragend: {
    on: (cb: () => void) => void
    off: (cb: () => void) => void
  }
}

export default function useVisualCommand({
  dataModel,
  updateBlocks,
  focusData,
  dragstart,
  dragend,
}: CommandVisualParams) {
  const { state, register, init } = useCommand()

  register({
    name: 'delete',
    keyboard: ['backspace', 'delete', 'ctrl+d'],
    execute: () => {
      const before = deepcopy(dataModel.value.blocks)
      const after = deepcopy(focusData.value.unFocus)

      return {
        redo: () => {
          updateBlocks(after)
        },
        undo: () => {
          updateBlocks(before)
        },
      }
    },
  })

  register({
    name: 'drag',
    init() {
      this.data = { before: null as null | VisualEditorBlockData[] }

      const handler = {
        dragstart: () => {
          this.data = { before: deepcopy(dataModel.value.blocks) }
        },
        dragend: () => {
          state.commands.drag()
        },
      }

      dragstart.on(handler.dragstart)
      dragend.on(handler.dragend)

      return () => {
        dragstart.off(handler.dragstart)
        dragend.off(handler.dragend)
      }
    },
    execute() {
      let before = deepcopy(this.data.before)
      let after = deepcopy(dataModel.value.blocks)

      return {
        redo: () => {
          updateBlocks(after)
        },
        undo: () => {
          updateBlocks(before)
        },
      }
    },
  })

  register({
    name: 'clear',
    execute: () => {
      let data = {
        before: deepcopy(dataModel.value.blocks),
        after: deepcopy([]),
      }
      return {
        redo: () => {
          updateBlocks(deepcopy(data.after))
        },
        undo: () => {
          updateBlocks(deepcopy(data.before))
        },
      }
    },
  })

  register({
    name: 'placeTop',
    keyboard: 'ctrl+up',
    execute: () => {
      let data = {
        before: deepcopy(dataModel.value.blocks),
        after: deepcopy(
          (() => {
            const { focus, unFocus } = focusData.value
            const maxIndex = unFocus.reduce((prev, block) => Math.max(prev, block.zIndex), -Infinity) + 1
            focus.forEach((block) => (block.zIndex = maxIndex))
            return deepcopy(dataModel.value.blocks)
          })()
        ),
      }
      return {
        redo: () => {
          updateBlocks(deepcopy(data.after))
        },
        undo: () => {
          updateBlocks(deepcopy(data.before))
        },
      }
    },
  })

  register({
    name: 'placeBottom',
    keyboard: 'ctrl+down',
    execute: () => {
      let data = {
        before: deepcopy(dataModel.value.blocks),
        after: deepcopy(
          (() => {
            const { focus, unFocus } = focusData.value
            let minIndex = unFocus.reduce((prev, block) => Math.min(prev, block.zIndex), Infinity) - 1

            if (minIndex < 0) {
              const dur = Math.abs(minIndex)
              unFocus.forEach((block) => (block.zIndex += dur))
              minIndex = 0
            }

            focus.forEach((block) => (block.zIndex = minIndex))
            return deepcopy(dataModel.value.blocks)
          })()
        ),
      }
      return {
        redo: () => {
          updateBlocks(deepcopy(data.after))
        },
        undo: () => {
          updateBlocks(deepcopy(data.before))
        },
      }
    },
  })

  register({
    name: 'updateBlock',
    execute: (newBlock: VisualEditorBlockData, oldBlock: VisualEditorBlockData) => {
      let blocks = deepcopy(dataModel.value.blocks || [])
      let data = {
        before: blocks,
        after: (() => {
          blocks = [...blocks]

          const index = dataModel.value.blocks.indexOf(oldBlock)

          if (index >= 0) {
            blocks.splice(index, 1, newBlock)
          }
          return deepcopy(blocks)
        })(),
      }
      return {
        redo: () => {
          updateBlocks(deepcopy(data.after))
        },
        undo: () => {
          updateBlocks(deepcopy(data.before))
        },
      }
    },
  })

  register({
    name: 'updateModelValue',
    execute: (val: VisualEditorModelValue) => {
      let data = {
        before: deepcopy(dataModel.value),
        after: deepcopy(val),
      }
      return {
        redo: () => {
          dataModel.value = data.after
        },
        undo: () => {
          dataModel.value = data.before
        },
      }
    },
  })

  register({
    name: 'selectAll',
    followQueue: false,
    keyboard: 'ctrl+a',
    execute: () => {
      return {
        redo: () => (dataModel.value.blocks || []).forEach((block) => (block.focus = true)),
      }
    },
  })

  init()

  return {
    undo: state.commands.undo,
    redo: state.commands.redo,
    delete: () => {
      if (!focusData.value.focus.length) return
      state.commands.delete()
    },
    clear: () => state.commands.clear(),
    placeTop: () => state.commands.placeTop(),
    placeBottom: () => state.commands.placeBottom(),
    updateBlock: (newBlock: VisualEditorBlockData, oldBlock: VisualEditorBlockData) =>
      state.commands.updateBlock(newBlock, oldBlock),
    updateModelValue: (val: VisualEditorModelValue) => state.commands.updateModelValue(val),
  }
}
