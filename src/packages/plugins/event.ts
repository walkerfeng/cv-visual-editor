type eventCallback = (arg?: any) => void

export default function createEvent() {
  let callbacks: eventCallback[] = []
  return {
    on(cb: eventCallback) {      
      callbacks.push(cb)
    },
    emit() {      
      callbacks.forEach((cb) => cb && cb())
    },
    off(offCallback: eventCallback) {
      callbacks = callbacks.filter((cb) => cb !== offCallback)
    },
  }
}
