import { onUnmounted } from 'vue'
import { KeyboardCode } from './keyboard-code'

interface Commander {
  name: string
  keyboard?: string | string[]
  followQueue?: boolean
  execute: CommandExecute
  init?: () => () => void
  data?: any
}

type CommandExecute = (...args: any[]) => { undo?: () => void; redo: () => void }

interface CommandState {
  queue: ReturnType<CommandExecute>[]
  current: number
  commands: Record<string, (...arg: any[]) => void>
  commandArray: Commander[]
  destroyList: Array<() => void>
}

export default function useCommand() {
  const state: CommandState = {
    queue: [],
    current: -1,
    commands: {},
    commandArray: [],
    destroyList: [],
  }

  const register = (command: Commander) => {
    state.commandArray.push(command)
    state.commands[command.name] = (...args: any[]) => {
      const { undo, redo } = command.execute(...args)
      redo()
      if (command.followQueue === false) return
      let { queue, current } = state
      if (queue.length) {
        queue = queue.slice(0, current + 1)
        state.queue = queue
      }
      state.queue.push({ undo, redo })
      state.current++
    }
  }

  const keyboardEvent = (() => {
    const onkeydown = (e: KeyboardEvent) => {
      if (document.activeElement !== document.body) return
      const { keyCode, shiftKey, altKey, ctrlKey, metaKey } = e

      let keyString: string[] = []
      if (ctrlKey || metaKey) keyString.push('ctrl')
      if (shiftKey) keyString.push('shift')
      if (altKey) keyString.push('alt')

      keyString.push(KeyboardCode[keyCode])
      const keyNames = keyString.join('+')
      state.commandArray.forEach(({ keyboard, name }) => {
        if (!keyboard) return
        const keys = Array.isArray(keyboard) ? keyboard : [keyboard]
        if (keys.indexOf(keyNames) > -1) {
          state.commands[name]()
          e.stopPropagation()
          e.preventDefault()
        }
      })
    }

    const init = () => {
      window.addEventListener('keydown', onkeydown)
      return () => window.removeEventListener('keydown', onkeydown)
    }

    return init
  })()

  const init = () => {
    const onKeydown = (e: KeyboardEvent) => {
      // 其它键盘事件
    }
    state.commandArray.forEach((command) => command.init && state.destroyList.push(command.init()))
    state.destroyList.push(keyboardEvent())
    state.destroyList.push(() => window.removeEventListener('keydown', onKeydown))
  }

  register({
    name: 'undo',
    keyboard: 'ctrl+z',
    followQueue: false,
    execute: () => {
      return {
        redo: () => {
          const queueItem = state.queue[state.current]
          if (queueItem && queueItem.undo) {
            queueItem.undo()
            state.current--
          }
        },
      }
    },
  })

  register({
    name: 'redo',
    keyboard: ['ctrl+y', 'ctrl+shift+z'],
    followQueue: false,
    execute: () => {
      return {
        redo: () => {
          const queueItem = state.queue[state.current + 1]
          if (queueItem) {
            queueItem.redo()
            state.current++
          }
        },
      }
    },
  })

  onUnmounted(() => state.destroyList.forEach((fn) => fn && fn()))

  return {
    state,
    register,
    init,
  }
}
