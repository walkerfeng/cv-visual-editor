import { ref, watch } from "vue";

export function useModel<T>(getter: () => T, emitter: (val: T) => void) {
    const state = ref(getter()) as { value: T };

    watch(getter, val => {
        if (val !== state.value) {
            state.value = val
        }
    })
    
    return {
        get value() {
            return state.value
        },
        set value(val: T) {
            state.value = val;
            emitter(val)
        }
    }
}

// import {defineComponent} from "vue"
// export const TestUseModel = defineComponent({
//     props:  {
//         modelValue: { type: String }
//     },
//     emit: {
//         "update:modelValue": (val?: string)=> true
//     },
//     setup(props,ctx){
//         const model = useModel(()=>props.modelValue,(val)=>ctx.emit("update:modelValue",val));
//         return ()=>(
//             <div>
//                 <input type="text" v-model={model.value} />
//             </div>
//         )
//     }
// })