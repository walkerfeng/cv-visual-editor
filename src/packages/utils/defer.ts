interface Defer {
  (): {
    resolve: () => void
    reject: () => void
    promise: Promise<void>
  }
  <T>(): {
    resolve: (val: T) => void
    reject: (val: T) => void
    promise: Promise<T>
  }
}

export const defer: Defer = () => {
  const dfd = {} as any
  dfd.promise = new Promise((resolve, reject) => {
    dfd.resolve = resolve
    dfd.reject = reject
  })

  return dfd
}

export const delay = (ms: number) => new Promise((r) => setTimeout(r, ms))
