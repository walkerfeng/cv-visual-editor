import { computed, defineComponent, onMounted, PropType, ref, Slot } from 'vue'
import { VisualEditorBlockData, VisualEditorConfig } from './visual-editor.utils'
import { BlockResiz } from '@/packages/components/block-resize/block-resize'

export const VisualEditorBlock = defineComponent({
  props: {
    block: { type: Object as PropType<VisualEditorBlockData>, required: true },
    config: { type: Object as PropType<VisualEditorConfig>, required: true },
    formData: { type: Object as PropType<Record<string, any>>, required: true },
    slots: { type: Object as PropType<Record<string, Slot | undefined>>, required: true },
    customProps: { type: Object as PropType<Record<string, any>> },
  },
  setup(props) {
    const el = ref({} as HTMLDivElement)

    const style = computed(() => ({
      top: props.block.top + 'px',
      left: props.block.left + 'px',
      zIndex: props.block.zIndex || 0,
    }))

    const classes = computed(() => [
      'visual-editor-block',
      {
        'visual-editor-block-focus': props.block.focus,
      },
    ])

    onMounted(() => {
      const block = props.block
      if (block.adjustPosition) {
        const { offsetWidth, offsetHeight } = el.value
        block.top = block.top - offsetHeight / 2
        block.left = block.left - offsetWidth / 2
        block.height = offsetHeight
        block.width = offsetWidth
        block.adjustPosition = false
      }
    })

    return () => {
      const component = props.config.componentMap[props.block.componentKey]
      const formData = props.formData

      let render: any
      if (props.block.slotName && props.slots[props.block.slotName]) {
        render = props.slots[props.block.slotName]!()
      } else {

        render = component.render({
          size: props.block.hasResize
            ? {
                width: props.block.width,
                height: props.block.height,
              }
            : {},
          props: props.block.props || {},
          model: Object.keys(component.model || {}).reduce((prev, propName) => {
            const modelName = props.block.model ? props.block.model[propName] : null
            prev[propName] = {
              [propName === 'default' ? 'modelValue' : propName]: modelName ? formData[modelName] : null,
              [propName === 'default' ? 'onUpdate:modelValue' : 'onChange']: (val: any) =>
                modelName && (formData[modelName] = val),
            }
            return prev
          }, {} as Record<string, any>),
          custom: !props.block.slotName || !props.customProps ? {} : props.customProps[props.block.slotName] || {},
        })
      }

      const { width, height } = component.resize || {}

      return (
        <div ref={el} class={classes.value} style={style.value}>
          {props.block.focus && (width || height) && (
            <BlockResiz block={props.block} component={component}></BlockResiz>
          )}
          {render}
        </div>
      )
    }
  },
})
