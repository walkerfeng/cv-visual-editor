module.exports = {
  outputDir: 'docs',
  publicPath: '/cv-visual-editor/',
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: ['./src/visual.styles.scss'],
    },
  },
}
